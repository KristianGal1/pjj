package cz.mendelu.xgal1.battleships;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LodTest {

    @Test
    void uplneZniceny() {
        //given
        Lod lod = new Lod(5);
        lod.zbytekCasti = 0;
        //then
        boolean result = lod.uplneZniceny();
        //when
        assertTrue(result == true);
    }

    @Test
    void uplneZniceny_vraci_false() {
        Lod lod = new Lod(2);
        lod.zbytekCasti = 2;
        //then
        boolean result = lod.uplneZniceny();
        //when
         assertTrue(result == false);
    }

    @Test
    void pridajBunku() {
        //given
        Lod lod = new Lod(5);
        Bunka bunka = new Bunka(2, 2);
        //then
        lod.pridajBunku(bunka);
        int result = lod.bunky.length;
        //when
        assertEquals(result, 6);
    }
        @Test
        void znicSa() {
            //given
            Lod l = new Lod(5);
            //then
                l.znicSa();
                l.znicSa();
                int result = l.bunky.length;
            //when
            assertEquals(1,result);
        }
}

