package cz.mendelu.xgal1.battleships;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HernePoleTest {


    HernePole hp;

    @BeforeEach
    void setup() {
        hp = new HernePole();
    }


    @Test
    void skus_na_suradniciach_je_lod() {
        //setup
        Bunka b = new Bunka(3,3);
        hp.pole[3][3] = b;
        //when
        boolean result = hp.skus(3,3);
        assertTrue(result == true);
    }

    @Test
    void skus_na_suradniciach_neni_lod() {
        //setup
        Bunka b = new Bunka(4,4);
        hp.pole[4][4] = b;
        //when
        boolean result = hp.skus(3,3);
        assertTrue(result == false);
    }

    @Test
    void skuska_ci_prazdne_pole_je_prazdne() {
        //setup
        hp.zostavajuce = 0;
        //when
        boolean result = hp.skuskaCiPrazdne();
        //then
        assertTrue(result == true);
    }

    @Test
    void skuska_ci_prazdne_pole_neni_prazdne() {
        //setup
        hp.zostavajuce = 5;
        //when
        boolean result = hp.skuskaCiPrazdne();
        //then
        assertTrue(result == false);
    }





}