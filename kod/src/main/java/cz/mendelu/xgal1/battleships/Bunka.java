package cz.mendelu.xgal1.battleships;

import java.util.Objects;

public class Bunka {
    public int riadok,stlpec;
    private Lod lod; //lode ktore bude bunka uschovavat

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bunka bunka = (Bunka) o;
        return riadok == bunka.riadok &&
                stlpec == bunka.stlpec &&
                Objects.equals(lod, bunka.lod);
    }

    @Override
    public String toString() {
        return "Bunka{" +
                "riadok=" + riadok +
                ", stlpec=" + stlpec +
                ", lod=" + lod +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(riadok, stlpec, lod);
    }

    public Bunka(int riadok, int stlpec){
        this.riadok = riadok;
        this.stlpec = stlpec;
    }

    public boolean obsahujeLod(){   //vrati hodnotu true ked bunka obsahuje lod
        return lod != null;
    }
    public void vymazLod(){
        lod = null;
    }
    public void setLod(Lod l){
        lod = l;
    }
    public Lod getLod(){
        return lod;
    }


}
