package cz.mendelu.xgal1.battleships;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Lod {
     int zbytekCasti;
     List<Bunka> bunky;

    public Lod(int velkost) {
        zbytekCasti = velkost;
        bunky = new ArrayList<>(velkost);
    }

    public void pridajBunku(Bunka b) {
        bunky.add(b);
    }

    public void znicSa() {
        zbytekCasti -= 1;
    }
    public boolean uplneZniceny(){
        return zbytekCasti <= 0;
    }

    @Override
    public String toString() {
        return "Lod{" +
                "zbytekCasti=" + zbytekCasti +
                ", bunky=" + bunky +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lod lod = (Lod) o;
        return zbytekCasti == lod.zbytekCasti &&
                Objects.equals(bunky, lod.bunky);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zbytekCasti, bunky);
    }
}